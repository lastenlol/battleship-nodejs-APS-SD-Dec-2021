const readline = require('readline-sync');
const cliColor = require('cli-color');
const beep = require('beepbeep');
const { random } = require('lodash');
const gameController = require('./GameController/gameController.js');
const position = require('./GameController/position.js');
const letters = require('./GameController/letters.js');

class Battleship {
  start() {
    console.log(cliColor.magenta('                                     |__'));
    console.log(cliColor.magenta('                                     |\\/'));
    console.log(cliColor.magenta('                                     ---'));
    console.log(cliColor.magenta('                                     / | ['));
    console.log(cliColor.magenta('                              !      | |||'));
    console.log(
      cliColor.magenta("                            _/|     _/|-++'"),
    );
    console.log(
      cliColor.magenta('                        +  +--|    |--|--|_ |-'),
    );
    console.log(
      cliColor.magenta('                     { /|__|  |/\\__|  |--- |||__/'),
    );
    console.log(
      cliColor.magenta(
        "                    +---------------___[}-_===_.'____                 /\\",
      ),
    );
    console.log(
      cliColor.magenta(
        "                ____`-' ||___-{]_| _[}-  |     |_[___\\==--            \\/   _",
      ),
    );
    console.log(
      cliColor.magenta(
        " __..._____--==/___]_|__|_____________________________[___\\==--____,------' .7",
      ),
    );
    console.log(
      cliColor.magenta(
        '|                        Welcome to Battleship                         BB-61/',
      ),
    );
    console.log(
      cliColor.magenta(
        ' \\_________________________________________________________________________|',
      ),
    );
    console.log();

    this.InitializeGame();
    this.StartGame();
  }

  StartGame() {
    console.clear();
    console.log('                  __');
    console.log('                 /  \\');
    console.log('           .-.  |    |');
    console.log("   *    _.-'  \\  \\__/");
    console.log("    \\.-'       \\");
    console.log('   /          _/');
    console.log('  |      _  /');
    console.log("  |     /_\\'");
    console.log('   \\    \\_/');
    console.log('    """"');

    let roundNumber = 1;

    do {
      console.log(`
=============
Round ${roundNumber}:
=============\n`);

      console.log(cliColor.yellow("  Player, it's your turn"));
      console.log(cliColor.yellow('  Enter coordinates for your shot :'));
      console.log();
      const position = Battleship.ParsePosition(readline.question());
      var isHit = gameController.CheckIsHit(this.enemyFleet, position);
      if (isHit) {
        beep();

        console.log(cliColor.red('                \\         .  ./'));
        console.log(cliColor.red('              \\      .:";\'.:.."   /'));
        console.log(cliColor.red("                  (M^^.^~~:.'\")."));
        console.log(cliColor.red('            -   (/  .    . . \\ \\)  -'));
        console.log(cliColor.red('               ((| :. ~ ^  :. .|))'));
        console.log(cliColor.red('            -   (\\- |  \\ /  |  /)  -'));
        console.log(cliColor.red('                 -\\  \\     /  /-'));
        console.log(cliColor.red('                   \\  \\   /  /'));
      }

      console.log();
      console.log(
        `  ${isHit ? cliColor.red('Yeah ! Nice hit !') : cliColor.blue('Miss')}`,
      );

      const computerPos = this.GetRandomPosition();
      var isHit = gameController.CheckIsHit(this.myFleet, computerPos);
      console.log();
      console.log(
        `  Computer shot in ${computerPos.column}${computerPos.row} and ${
          isHit ? cliColor.red('has hit your ship !') : cliColor.blue('miss')
        }`,
      );
      if (isHit) {
        beep();

        console.log(cliColor.red('                \\         .  ./'));
        console.log(cliColor.red('              \\      .:";\'.:.."   /'));
        console.log(cliColor.red("                  (M^^.^~~:.'\")."));
        console.log(cliColor.red('            -   (/  .    . . \\ \\)  -'));
        console.log(cliColor.red('               ((| :. ~ ^  :. .|))'));
        console.log(cliColor.red('            -   (\\- |  \\ /  |  /)  -'));
        console.log(cliColor.red('                 -\\  \\     /  /-'));
        console.log(cliColor.red('                   \\  \\   /  /'));
      }

      roundNumber += 1;
    } while (true);
  }

  static ParsePosition(input) {
    const letter = letters.get(input.toUpperCase().substring(0, 1));
    const number = parseInt(input.substring(1, 2), 10);
    return new position(letter, number);
  }

  GetRandomPosition() {
    const rows = 8;
    const lines = 8;
    const rndColumn = Math.floor(Math.random() * lines);
    const letter = letters.get(rndColumn + 1);
    const number = Math.floor(Math.random() * rows);
    const result = new position(letter, number);
    return result;
  }

  InitializeGame() {
    this.InitializeMyFleet();
    this.InitializeEnemyFleet();
  }

  InitializeMyFleet() {
    this.myFleet = gameController.InitializeShips();

    console.log(
      cliColor.yellow(
        'Please position your fleet (Game board size is from A to H and 1 to 8) :',
      ),
    );

    const setOfPositions = new Set();

    this.myFleet.forEach((ship) => {
      function check(position) {
        if (!/[A-H][1-8][RD]/i.test(position)) return false;
        return true;
      }

      let position = '';

      do {
        console.log();
        console.log(
          cliColor.yellow(
            `Please enter the positions for the ${ship.name} (size: ${ship.size}, format: A1(R|D) where R stands for Right, D – for Down)`,
          ),
        );
        console.log();
        position = readline.question().toString().toUpperCase();
        if (!check(position)) {
          console.log(cliColor.red('Wrong format of input, try again please'));
          continue;
        }
        const [x, y, z] = position.split('');

        const addedPositions = [];

        for (let j = 0; j < ship.size; j += 1) {
          const xPos = z === 'R' ? String.fromCharCode(x.charCodeAt(0) + j) : x;
          const yPos = Number(z === 'D' ? Number(y) + j : y);
          const newPos = xPos + yPos;

          addedPositions.push(newPos);

          if (
            !newPos ||
            setOfPositions.has(newPos)
            || !/[a-h]/i.test(xPos)
            || yPos < 1
            || yPos > 8
          ) {
            if (setOfPositions.has(newPos)) {
              console.log(cliColor.red('Ship has intersection with others'));
            }
            if (!/[a-h]/i.test(xPos) || yPos < 1 || yPos > 8) {
              console.log(cliColor.red('Ship is out of bounds'));
            }
            ship.positions = [];
            position = '';

            for (const addedPos of addedPositions) {
              setOfPositions.delete(addedPos);
            }

            break;
          }

          setOfPositions.add(newPos);

          ship.addPosition(Battleship.ParsePosition(newPos));
        }
      } while (!check(position));
    });
  }

  InitializeEnemyFleet() {
    const positionsSets = [
      [
        ['B2', 'B3', 'B4', 'B5', 'B6'],
        ['C2', 'D2', 'E2', 'F2'],
        ['E5', 'E6', 'E7'],
        ['H5', 'H6', 'H7'],
        ['H2', 'H3'],
      ],
      [
        ['B2', 'B3', 'B4', 'B5', 'B6'],
        ['E3', 'F3', 'G3', 'H3'],
        ['B8', 'C8', 'D8'],
        ['H6', 'H7', 'H8'],
        ['H2', 'H3'],
      ],
      [
        ['B2', 'B3', 'B4', 'B5', 'B6'],
        ['F3', 'F4', 'F5', 'F6'],
        ['A8', 'B8', 'C8'],
        ['H5', 'H6', 'H7'],
        ['G1', 'H1'],
      ],
      [
        ['A2', 'A3', 'A4', 'A5', 'A6'],
        ['C1', 'D1', 'E1', 'F1'],
        ['H3', 'H4', 'H5'],
        ['E5', 'E6', 'E7'],
        ['C7', 'D7'],
      ],
      [
        ['C2', 'C3', 'C4', 'C5', 'C6'],
        ['F2', 'F3', 'F4', 'F6'],
        ['E1', 'F1', 'G1'],
        ['E8', 'F8', 'G8'],
        ['G6', 'G7'],
      ],
    ];

    this.enemyFleet = gameController.InitializeShips();

    const n = random(0, 4);

    const positions = positionsSets[n];
    positions.forEach((fleets, i) => {
      fleets.forEach((positionStr) => {
        const [letter, index] = positionStr.split('');

        console.log(index, letter);

        this.enemyFleet[i].addPosition(new position(letters[letter], index));
      });
    });
  }
}

module.exports = Battleship;
