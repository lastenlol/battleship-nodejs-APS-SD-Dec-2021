const assert = require('assert').strict;
const cliColor = require('cli-color');
const gameController = require('../GameController/gameController.js');
const letters = require('../GameController/letters.js');
const position = require('../GameController/position.js');

describe('checkIsHitTests', () => {
  it('should return true if there is a ship at the shooting position', () => {
    const ships = gameController.InitializeShips();
    counter = 1;
    ships.forEach((ship) => {
      for (let i = 1; i <= ship.size; i++) {
        column = letters.get(counter);
        ship.addPosition(new position(letters.get(counter), i));
      }
      counter++;
    });
    const actual = gameController.CheckIsHit(ships, new position(letters.B, 3));
    assert.ok(actual);
  });

  it('should return false if there is no ship at the shooting position', () => {
    const ships = gameController.InitializeShips();
    counter = 1;
    ships.forEach((ship) => {
      for (let i = 1; i <= ship.size; i++) {
        ship.addPosition(new position(letters.get(counter), i));
      }
      counter++;
    });
    const actual = gameController.CheckIsHit(ships, new position(letters.G, 1));
    assert.strictEqual(actual, false);
  });

  it('should throw an exception if positstion is undefined', () => {
    const ships = gameController.InitializeShips();
    assert.throws(
      () => {
        const actual = gameController.CheckIsHit(ships, undefined);
        return actual;
      },
    );
  });

  it('should throw an exception if ship is undefined', () => {
    assert.throws(
      () => {
        const actual = gameController.CheckIsHit(undefined, new position(letters.G, 1));
        return actual;
      },
    );
  });

  it('The text is colored', () => {
    assert.ok(cliColor.red('The text is colored') === '\x1B[31mThe text is colored\x1B[39m');
  });
});
