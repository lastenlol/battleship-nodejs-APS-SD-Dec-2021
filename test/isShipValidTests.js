const assert = require('assert').strict;
const gameController = require('../GameController/gameController.js');
const letters = require('../GameController/letters.js');
const position = require('../GameController/position.js');
const ship = require('../GameController/ship');

describe('isShipValidTests', () => {
  it('should return true if the ship is valid', () => {
    const testship = new ship('Battleship', 3, 0);
    testship.addPosition(new position(letters.A, 1));
    testship.addPosition(new position(letters.A, 2));
    testship.addPosition(new position(letters.A, 3));

    const actual = gameController.isShipValid(testship);
    assert.ok(actual);
  });

  it('should return false if the ship is invalid', () => {
    const testship = new ship('Battleship', 3, 0);

    const actual = gameController.isShipValid(testship);
    assert.ok(!actual);
  });
});
