const {
  Given, When, Then, defineParameterType,
} = require('@cucumber/cucumber');
const assert = require('assert').strict;
const shipDef = require('../../GameController/ship.js');
const position = require('../../GameController/position.js');
const letters = require('../../GameController/letters.js');
const gameController = require('../../GameController/gameController.js');

let ship;
let actual;

defineParameterType({ name: 'bool', regexp: /"([^"]*)"/, transformer(text) { return text.toLowerCase() == 'true'; } });

Given('I have a {int} ship with {int} positions', (size, positions) => {
  ship = new shipDef();
  ship.size = size;
  for (let i = 1; i <= positions; i++) {
    ship.addPosition(new position(letters.A, i));
  }
});

When('I check if the ship is valid', () => {
  actual = gameController.isShipValid(ship);
});

Then('the result should be {bool}', (expected) => {
  assert.strictEqual(actual, expected);
});
